<?php
$colors_file = simplexml_load_file('./colors.xml');

foreach ($colors_file->color as $color){
    $key = (string)$color['name'];
    $value = (string)$color;
    $colors[$key] = $value;
}

$colors_json = json_encode($colors);
echo $colors_json;