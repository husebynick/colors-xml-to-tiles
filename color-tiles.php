<?php
$rootPath = '../';
$title = "Color Tiles - nickhuseby.xyz";
$addedScript = '<script src="https://unpkg.com/vue@2.6.8/dist/vue.min.js"></script>'
    . '<link rel="stylesheet" href="' . $rootPath . 'css/animation.css">'
    . '<link rel="stylesheet" href="./css/tiles-styles.css">';
include($rootPath . "head.inc.php");
?>
<h1 class="section-heading theme-blue main-div">Color Tiles</h1>
<h3 class="mid-blue main-div"><i>Using Android colors.xml</i></h3>
<div class="main-div" id="tiles">
    <div class="main-div"><b>Selected Colors</b></div>
        <div class="colors-container">
            <div v-if="selectedColors.length == 0" class="dark-blue">No colors selected.</div>
            <div v-for="(color, index) in selectedColors" class="tile" :key="index">
                <div :style="{ backgroundColor: color[1], height: '125px', width: '100%', borderTopRightRadius: '15px', borderTopLeftRadius: '15px'}"></div>
                <div style="margin-top: 15px;">{{ color[0] }}</div>
                <div>{{ color[1] }}</div>
            </div>
        </div>
    <div class="main-div"><b>Filters</b></div>
    <div style="text-align: center;"> 
        <button class="button" style="margin: 5px;" v-for="(color, index) in colorOptions" :key="index" @click="colorOpen = color">{{ capName(color) }}</button>
    </div>
    <div class="colors-container">
        <color_tile v-for="(color, index) in colorsJSON" 
                    :key="index" :data-index="index" 
                    :color="color[1]" 
                    :name="formatName(color[0])" 
                    v-show="inColors(color)"
                    @color-select="manageSelected"></color_tile>
    </div>           
</div>
<script>

const color_tile = {
    data() {
        return{
            selected: false,
        }
    },
    props: ['color', 'name'],
    methods: {
        selectColor(){
            this.selected = this.selected ? false : true;
            this.$emit("color-select", { addSelected: this.selected, el: [this.name, this.color]});
        }
    },
    template: `
        <div class="tile" @click="selectColor">
            <div :style="{ backgroundColor: color, height: '125px', width: '100%', borderTopRightRadius: '15px', borderTopLeftRadius: '15px'}"><i class="icon fancy-check ion-md-checkmark" v-show="selected"></i></div>
            <div style="margin-top: 15px;">{{ name }}</div>
            <div>{{ color }}</div>
        </div>
    `
}
    
const vm = new Vue({
    el: "#tiles",
    data: {
        colorsJSON: {},
        colorOpen: "all",
        colorOptions: [
            "all",
            "reds",
            "oranges",
            "yellows",
            "greens",
            "blues",
            "purples",
            "pinks",
            "greys"
        ],
        selectedColors: [],
    },
    methods: {
        formatName(name) {
            name = name.toUpperCase();
            name = name.split("_");
            var fn = "";
            for (var i = 0; i < name.length; i++){
                fn += name[i].substr(0,1) + name[i].substr(1).toLowerCase() + " ";
            }
            return fn.trim();
        },
        capName(name) {
            return name.substr(0,1).toUpperCase() + name.substr(1);
        },
        inColors(arr) {
            return this.colors.includes(arr);
        },
        manageSelected(pay){
            if (pay.addSelected) {
                this.selectedColors.push(pay.el);
            } else {
                this.selectedColors.splice(this.selectedColors.findIndex(function(item){
                   return item[1] == pay.el[1]; 
                }), 1);
            }
        }
    },
    computed: {
        colors: function() {
            var colors;
            if (this.colorOpen == "all") colors = this.colorsJSON;
            else colors = this.colorsJSON.filter(color => {
                var c = color[1].replace("#", "");
                var red = parseInt(c.substr(0,2), 16);
                var green = parseInt(c.substr(2,2), 16);
                var blue = parseInt(c.substr(4,2), 16);
                red /= 255;
                green /= 255;
                blue /= 255;
                var hue;
                if ((red >= green) && (green >= blue)) hue = ((green-blue)/(red-blue));
                if ((green > red) && (red >= blue)) hue = (2-((red-blue)/(green-blue)));
                if ((green >= blue) && (blue > red)) hue = (2+((blue-red)/(green-red)));
                if ((blue > green) && (green > red)) hue = (4-((green-red)/(blue-red)));
                if ((blue > red) && (red >= green)) hue = (4+((red-green)/(blue-green)));
                if ((red >= blue) && (blue > green)) hue = (6-((blue-green)/(red-green)));
                hue *= 60;
                switch (this.colorOpen){
                    case "reds":
                        if (hue < 20 || hue > 351) return color;
                        break;
                    case "oranges":
                        if (hue < 40 && hue > 21) return color;
                        break;
                    case "yellows":
                        if (hue < 71 && hue > 40) return color;
                        break;
                    case "greens":
                        if (hue > 80 && hue < 180) return color;
                        break;
                    case "blues":
                        if (hue > 180 && hue < 249) return color;
                        break;
                    case "purples":
                        if (hue < 310 && hue > 249) return color;
                        break;
                    case "pinks":
                        if (hue > 310 && hue < 351) return color;
                        break;
                    case "greys":
                        if ((red == blue) && (blue == green)) return color;
                        break;
                    default: 
                        console.log("Problem!")
                }                 
            });
            return colors;
        }
    },
    components: {
        "color_tile": color_tile,
    },
    mounted() {
        var colors = fetch("./colors.php")
            .then(res => {
                return res.json();
            })
            .then(json => {
                this.colorsJSON = Object.entries(json);
                console.log(this.colorsJSON)
            })
    }
});
</script>
<?php
include($rootPath . "foot.inc.php");
?>